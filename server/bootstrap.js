require("ignore-styles");
require("url-loader");
require("@babel/polyfill");
require("@babel/register");

const fs = require("fs");
const path = require("path");
const md5File = require("md5-file");
const mime = require("mime-types");
const register = require("ignore-styles").default;

// /**
//  * reads through all the images and either converts them to base64
//  * or computes the md5 hash and appends to the end of the file name.
//  *
//  */
register(undefined, (mod, filename) => {
    const ext = [".png", ".jpg", ".svg"].find(f => filename.endsWith(f));
    if (!ext) return;
    if (fs.statSync(filename).size < 10000) {
        const file = fs.readFileSync(filename).toString("base64");
        const mimeType = mime.lookup(ext) || "image/jpg";
        mod.exports = `data:${mimeType};base64,${file}`;
    } else {
        const hash = md5File.sync(filename).slice(0, 8);
        const bn = path.basename(filename).replace(/(\.\w{3})$/, `.${hash}$1`);
        mod.exports = `/static/media/${bn}`;
    }
});

require("./server");
