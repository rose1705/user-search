import express from "express";
import router from "./router";

const app = express();
const PORT = 4000;

// ensure all http traffic is directed to httpS
app.use((request, response, next) => {
    !request.secure &&
        request.get("X-Forwarded-Proto") !== "https" &&
        request.protocol === "http" &&
        response.redirect(`https://${request.headers.host}${request.url}`);

    next();
});

app.use(router);

app.listen(PORT, () => console.log(`express running on port ${PORT}`));
