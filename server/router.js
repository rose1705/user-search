import express from "express";
import path from "path";
import renderReact from "./renderer";

const router = express.Router();

router.use("^/$", renderReact());

router.use(express.static(path.resolve(__dirname, "..", "build")));

export default router;
