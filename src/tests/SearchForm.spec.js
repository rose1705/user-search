import React from "react";
import { shallow } from "enzyme";
import SearchForm from "../components/SearchForm/SearchForm";

describe("SearchForm", () => {
    it("should match snapshot", () => {
        const wrapper = shallow(
            <SearchForm
                handleFormSubmit={jest.fn()}
                handleInputChange={jest.fn()}
            />,
        );
        expect(wrapper).toMatchSnapshot();
    });
    it("should call handleInputChange when input field is edited", () => {
        const handleInputChange = jest.fn();
        const wrapper = shallow(
            <SearchForm
                handleFormSubmit={jest.fn()}
                handleInputChange={handleInputChange}
            />,
        );

        const inputField = wrapper.find("input");
        inputField.simulate("change", { currentTarget: { value: "test" } });

        expect(handleInputChange).toHaveBeenCalled();
    });
    it("should call handleFormSubmit when search button is clicked", () => {
        const handleFormSubmit = jest.fn();
        const wrapper = shallow(
            <SearchForm
                handleFormSubmit={handleFormSubmit}
                handleInputChange={jest.fn()}
            />,
        );

        const form = wrapper.find("form");
        form.simulate("submit", { preventDefault: jest.fn() });

        expect(handleFormSubmit).toHaveBeenCalled();
    });
});
