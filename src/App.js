import React from "react";
import "./styles/App.scss";
import Header from "./components/Header/Header";
import SearchForm from "./components/SearchForm/SearchForm";
import { Route } from "react-router-dom";
import UserList from "./components/Results/UserList";
import { getAllUsers, findSingleUser } from "./config/apiClient";
import ContactInformation from "./components/Results/ContactInformation";

//TODO: rewrite to use hook (simplifies code)
//TODO: figure out why gitlab pages gives a blank page
class App extends React.Component {
    state = {
        username: "",
        user: {},
        users: [],
    };

    async componentDidMount() {
        const users = await getAllUsers();
        this.setState({ users });
    }

    handleInputChange = e => {
        const { name, value } = e.currentTarget;
        this.setState({ [name]: value });
    };

    //TODO: clean up function to only perform one action
    handleFormSubmit = async e => {
        e.preventDefault();
        const { username } = this.state;
        if (username === "") {
            const users = await getAllUsers();
            this.setState({ users });
        } else {
            const user = await findSingleUser(username);
            this.setState({ users: [user] });
        }
    };

    render() {
        const { users } = this.state;
        return (
            <div className="App">
                <Header />
                <SearchForm
                    handleFormSubmit={this.handleFormSubmit}
                    handleInputChange={this.handleInputChange}
                />
                <Route
                    path="/:username"
                    render={props => <ContactInformation {...props} />}
                />

                <Route
                    exact
                    path="/"
                    render={() => <UserList users={users} />}
                />
            </div>
        );
    }
}

export default App;
