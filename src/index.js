import React from "react";
import { hydrate, render } from "react-dom";
import "./styles/index.scss";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route } from "react-router-dom";
import Alert from "react-s-alert";

// ReactDOM.render(
//     <BrowserRouter>
//         <Route path="/" component={App} />
//         <Alert stack={{ limit: 3 }} effect={"scale"} />
//     </BrowserRouter>,
//     document.getElementById("root"),
// );

const root = document.getElementById("root");

root.hasChildNodes()
    ? hydrate(
          <BrowserRouter>
              <Route path="/" component={App} />
              <Alert stack={{ limit: 3 }} effect={"scale"} />
          </BrowserRouter>,
          root,
      )
    : render(
          <BrowserRouter>
              <Route path="/" component={App} />
              <Alert stack={{ limit: 3 }} effect={"scale"} />
          </BrowserRouter>,
          root,
      );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
