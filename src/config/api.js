const BASE_URL = `https://api.github.com`;

export default {
    users: () => `${BASE_URL}/users`,
    findUser: username => `${BASE_URL}/users/${username}`,
};
