import api from "./api";
import Alert from "react-s-alert";

//TODO: improve error handling

const getHeader = {
    Accept: "application/vnd.github.v3+json",
};

export const getAllUsers = async () => {
    try {
        const response = await fetch(api.users(), getHeader);
        const data = await response.json();
        return response.ok && data;
    } catch (e) {
        if (e.response.status === 403) {
            return Alert.error(
                "Failed to get all users: you have reached the rate limit.",
            );
        }

        Alert.error("Failed to get all users. Please try again later.");
    }
};

export const findSingleUser = async username => {
    try {
        const response = await fetch(api.findUser(username), getHeader);
        const data = await response.json();
        return response.ok && data;
    } catch (e) {
        if (e.response.status === 403) {
            return Alert.error(
                "Failed to search for user: you have reached the rate limit.",
            );
        }

        Alert.error("Failed to search for user. Please try again later.");
    }
};
