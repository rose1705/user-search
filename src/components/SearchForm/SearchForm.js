import React from "react";
import "../../styles/SearchForm.scss";

const SearchForm = ({ handleFormSubmit, handleInputChange }) => (
    <div className="SearchForm">
        <h2>Search for GitHub User</h2>
        <form onSubmit={handleFormSubmit}>
            <fieldset>
                <legend>Type username here</legend>
                <label>
                    <input name="username" onChange={handleInputChange} />
                </label>
            </fieldset>
            <button type="submit">Search</button>
        </form>
    </div>
);

export default SearchForm;
