import React from "react";
import { Link } from "react-router-dom";
import "../../styles/ContactInformation.scss";

//TODO: create general function for returning user data
//TODO: create solution for html table display to make it less cluttered
//TODO: redirect to search results for new search with user details open

class ContactInformation extends React.Component {
    componentDidMount() {
        const { location: { state } = {} } = this.props;

        if (!state || !state.user) return this.props.history.push("/");
    }

    render() {
        const { location: { state } = {} } = this.props;

        return (
            <div className="ContactInformation">
                <div className="ContactInformation__header">
                    <h3> User data</h3>
                    <Link to="/">Back</Link>
                </div>
                <table className="ContactInformation__data">
                    <tbody>
                        <tr>
                            <td>Login Name</td>
                            <td>
                                {state.user.login
                                    ? state.user.login
                                    : "data not known"}
                            </td>
                        </tr>
                        <tr>
                            <td>E-mail</td>
                            <td>
                                {state.user.email
                                    ? state.user.email
                                    : "data not known"}
                            </td>
                        </tr>
                        <tr>
                            <td>GitHub</td>
                            <td>
                                {state.user.html_url ? (
                                    <a href={`${state.user.html_url}`}>
                                        {state.user.html_url}
                                    </a>
                                ) : (
                                    "data not known"
                                )}
                            </td>
                        </tr>
                        <tr>
                            <td>Blog</td>
                            <td>
                                {state.user.blog
                                    ? state.user.blog
                                    : "data not known"}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default ContactInformation;
