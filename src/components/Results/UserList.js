import React from "react";
import { Link } from "react-router-dom";
import arrow from "../../images/iconmonstr-arrow-27.svg";
import "../../styles/UserList.scss";

//TODO: add pagination and figure for readableStream
//TODO: add message / solution for empty user search

const setLink = user => ({
    pathname: `/${user.login}`,
    state: { user },
});

const UserList = ({ users = [] }) => (
    <ul className="UserList">
        {users.map(user => (
            <li key={user.login} className="UserList__item">
                <Link to={setLink(user)}>
                    <span>{user.login}</span>
                    <img src={arrow} alt="arrow" />
                </Link>
            </li>
        ))}
    </ul>
);

export default UserList;
