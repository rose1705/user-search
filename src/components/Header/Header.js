import React from "react";
import gitHubLogo from "../../images/GitHub-Mark-Light-120px-plus.png";

const Header = () => (
    <header className="App-header">
        <p>GitHub User Search</p>
        <a
            href="https://developer.github.com/v3/"
            target="_blank"
            rel="noopener noreferrer"
        >
            <img src={gitHubLogo} alt="github logo" />
        </a>
    </header>
);

export default Header;
